// Programme source exemple pour le TP d'Architectures specialisées M1 informatique

// à compiler avec le Makefile fourni : make TP_exemple
// Ce MakeFile est claqué mais on ne devait pas le changer

// Complété par mes soins : Quentin RAYMONDAUD
// Ce TP a sauvé ma matière "ARCHITECTURES SPECIALISEES"
// Une performance valuée à 16/20 par la matrice de Université Toulouse 3 - Paul Sabatier

// BON COURAGE !
// INUTILISABLE DEPUIS QUE OPENCV NE FOURNIT PLUS L'API C DANS LES DEPOTS
// A CONVERTIR EN C++ 


#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>
//#include <xmmintrin.h>	// SSE
//#include <emmintrin.h>	// SSE2
//#include <pmmintrin.h>	// SSE3
//#include <tmmintrin.h>	// SSSE3
#include <smmintrin.h>

#include <iomanip>

#define _mm_cmpge_epu8(a, b) \
		_mm_cmpeq_epi8(_mm_max_epu8(a, b), a)

#define _mm_cmple_epu8(a, b) _mm_cmpge_epu8(b, a)

#define _mm_cmpgt_epu8(a, b) \
		_mm_xor_si128(_mm_cmple_epu8(a, b), _mm_set1_epi8(-1))

#define _mm_cmplt_epu8(a, b) _mm_cmpgt_epu8(b, a)


using namespace std;

// affiche_xmm_i() -----------------------------------------------------//
// affiche le contenu du registre xmm entier sous différents format :	  //
//  8, 16 et 32 bits non signés.					//
//----------------------------------------------------------------------//

void affiche_xmm_i ( __m128i x )
{
	uint8_t *val8 = ( uint8_t * ) &x;										// Cast __m128i en tableau de uint8_t


	// AFFICHAGES 
	printf(" 8 bits : %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i \n", 
			 val8[0], val8[1], val8[2], val8[3], val8[4], val8[5], 
			 val8[6], val8[7], val8[8], val8[9], val8[10], 
			 val8[11],val8[12],val8[13],val8[14],val8[15]);
 
	uint16_t *val16 = (uint16_t*) &x;
	printf(" 16 bits : %i %i %i %i %i %i %i %i \n", 
			 val16[0], val16[1], val16[2], val16[3], val16[4], val16[5], 
			 val16[6], val16[7]);

	uint32_t *val32 = (uint32_t*) &x;
	printf(" 32 bits : %i %i %i %i  \n", 
			 val32[0], val32[1], val32[2], val32[3]);

	printf("\n");
}


// Inverse() -----------------------------------------------------------//
// inverse les valeurs des pixels de l'image en niveaux de gris X	//
// de taille h*l et renvoie le résultat dans Y.				//
//----------------------------------------------------------------------//

void Inverse(unsigned char *X, long h, long l, unsigned char *Y)
{
	int nPixels = h*l;
	for (int i=0; i < nPixels; i++){
		Y[i] = 255-X[i];
	}
}

void Inverse_sse(unsigned char *X, long h, long l, unsigned char *Y)
{

	int nbPixels = h * l;
	__m128i a , b , n;
	n = _mm_set1_epi8(255);						  // Créer un vecteur de 16 uint8_t initialisé a 255
	for ( int  i = 0  ; i < nbPixels ; i += 16 )  // 16 = 128/sizeof(char=8bits) pour faire l'opération bloc par bloc
	{											  // Avec 128 bits nous pouvons traiter maximum 16 elements en même temps 
		a = _mm_load_si128( (__m128i * ) &X[i]);  // On charge dans a
		b =  _mm_sub_epi8(n,a);					  // On soustrait la valeur actuelle a 255 sur tous les blocs
		_mm_store_si128( (__m128i*) &Y[i],b);	  // On stocke le résultat
	}
}


// Luminance_sse() -------------------------------------------------------------//
// réduit ou augmente de la valeur du paramètre d_lum les valeurs des pixels de	//
// l'image en niveaux de gris X de taille h*l et renvoie le résultat dans Y.	//
//------------------------------------------------------------------------------//

void Luminance_sse(int d_lum, unsigned char *X, long h, long l, unsigned char *Y)
{
	// FONCTION POURRAIT ÊTRE AMELIOREE EN COMPLEXITE EN SORTANT LE TEST DE LA BOUCLE
	// JE NE LE FAIT PAS PARCE QUE J'SUIS DEBILE
	int nbPixels = h * l;
	__m128i a , b , n;

	if ( d_lum >= 0)
		n = _mm_set1_epi8(d_lum); 	// Créer un vecteur de 16 uint8_t initialisé a d_lum
	else
		n = _mm_set1_epi8(-d_lum);	// Créer un vecteur de 16 uint8_t initialisé a | d_lum | 


	// / ! \ IL FAUT SORTIR LE TEST DE LA BOUCLE !
	// J'ai laissé ça comme parce que théoriquement il faut pas le faire mais en pratique ça semble marcher et en plus de ça, ça me sauve quelques lignes dans le code source !
	for ( int i = 0 ; i < nbPixels ; i += 16) 	  // 16 = 128/sizeof(char=8bits) 
	{
		a = _mm_load_si128( (__m128i *) &X[i] ) ; // Charge un bloc 16 * 8bits
		if ( d_lum >= 0) 						  // SI on ajoute de la luminance
			b = _mm_adds_epu8(a,n);				  // ALORS on additionne
		else									  // SINON on soustrait		
			b = _mm_subs_epu8(a,n);				 

		_mm_store_si128( (__m128i*) &Y[i] , b);	  // On stocke le resultat
	}

}


// binarise() ----------------------------------------------------------//
// binarise l'image en niveaux de gris X selon le paramètre seuil	//
// et renvoie le résultat dans Y.					//
//----------------------------------------------------------------------//

void binarise (unsigned char seuil, unsigned char *X, long h, long l, unsigned char *Y)
{
	int nPixels = h*l;
	for (int i=0; i < nPixels; i++)
	{
		 Y[i] = (X[i]>seuil)?255:0;
	}
}


void binarise_sse (unsigned char seuil, unsigned char *X, long h, long l, unsigned char *Y)
{
	int nbPixels = h*l;
	__m128i a , b , n ,max ;
	max = _mm_set1_epi8(( unsigned char ) 255);		// Créer un vecteur de 16 uint8_t initialisé a 255
	n = _mm_set1_epi8(seuil);						// Créer un vecteur de 16 uint8_t initialisé a seuil
	for ( int i = 0 ; i < nbPixels ; i += 16) {		// 16 = 128/sizeof(char=8bits) 
		a = _mm_load_si128( (__m128i *) &X[i] ); 	// Charge un bloc
		b = _mm_cmpgt_epu8( a , n ); 				// Permet d'obtenir le filtre sur le bloc
		b = _mm_and_si128( b , max); 				// Applique filtre 
		_mm_store_si128( (__m128i*) &Y[i] , b); 	// Stocke le bloc filtré
	}

}



// min_et_max() ----------------------------------------------------------------//
// calcule les valeurs min et max des pixels de l'image en niveaux de gris X.	//
//------------------------------------------------------------------------------//

void min_et_max (unsigned char *X, long h, long l, unsigned char & min, unsigned char & max ){
	int nPixels = h*l;
	min = 255;
	max = 0;

	for (int i=0; i < nPixels; i++)
	{
		if (X[i]<min)
			min=X[i];
		if (X[i]>max)
			max=X[i];
	}
}

unsigned char max_in_m128i( __m128i toExplore ){ // Horizontal search

	unsigned char * te = ( unsigned char * ) &toExplore;
	unsigned char max = 0;
	for (int i=0; i < 16; i++)
	if ( max < te[i])
		max = te[i];
	return max;
}
unsigned char min_in_m128i( __m128i toExplore ){ // Horizontal search

	unsigned char * te = ( unsigned char * ) &toExplore;
	unsigned char min = 255;
	for (int i=0; i < 16; i++)
	if ( min > te[i])
		min = te[i];
	return min;
}

void min_et_max_sse (unsigned char *X, long h, long l, unsigned char & min, unsigned char & max ) {
	int nbPixels = h*l;
	__m128i a , minV, maxV;
	minV = _mm_set1_epi8(min);							// Créer un vecteur de 16 uint8_t initialisé a min
	maxV = _mm_set1_epi8(max);							// Créer un vecteur de 16 uint8_t initialisé a max
	for ( int i = 0 ; i < nbPixels ; i += 16) {			// 16 = 128/sizeof(char=8bits) 
		a = _mm_load_si128( (__m128i * ) &X[i]);		// Chargement du bloc
		minV = _mm_min_epu8( a , minV); 				// PAS EVIDENT DE SAVOIR CE QUE FAIT RELLEMENT LA FONCTION !
		maxV = _mm_max_epu8( a , maxV); 				// d'après ce que j'ai observé elle retourn le bloc contenant la valeur MIN / MAX
	}
	min = min_in_m128i(minV); 							// recherche horizontale sur le bloc contenant la valeur min
	max = max_in_m128i(maxV); 							// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ max

}



// recadrageDynamique() ------------------------------------------------//
// calcule le recadrage dynamique de l'image en niveaux de gris X	//
// et renvoie le résultat dans Y					//
//----------------------------------------------------------------------//

void recadrageDynamique (unsigned char *X, long h, long l, unsigned char min, unsigned char max, unsigned char *Y)
{
	long i;
	long size=h*l;

	//calcule des paramètres pour le recadrage dynamique
	int div=max-min;
	for(i=0;i<size;i++){
		Y[i]=((X[i]-min)*255)/div;
	}
}


void recadrageDynamique_sse(unsigned char *X, long h, long l, unsigned char min, unsigned char max, unsigned int *Y){
	int nbPixels = h*l;

	__m128i a,minV,maxVT;
	__m128 b,div;
	minV = _mm_set1_epi32(min);				// Initialise un vecteur de 8 uint32_t à min
	maxVT = _mm_set1_epi32(255);			// Initialise un vecteur de 8 uint32_t à 255
	div = _mm_set1_ps((float)(max-min));	// Initialise un vecteur de 8 float simple précision  à max-min

	for(long i=0; i<nbPixels; i+=4){		// 4 =  128/sizeof(uint32_t)

		a = _mm_loadu_si128( (__m128i *) (uint32_t *) & X[i]); // Charge un bloc de 4 uint8_t casté en uint32_t
		a = _mm_sub_epi32(a,minV);						   	   // Soustrait min 
		a = _mm_mul_epu32(a,maxVT);							   // Multiplie par max
		b = _mm_cvtepi32_ps(a);								   // Cast en vecteur de float
		b = _mm_div_ps(b,div);								   // Divise par max-min
		a = _mm_cvtps_epi32(b);							   	   // Cast en vecteur de uint32_t
		_mm_store_si128( (__m128i *) &Y[i],  a);		   	   // CORE DUMP Casse tout =/
		// A l'époque je savais exactement pourquoi ca marchait pas, maintenant je me souviens juste du prof me dire " FALLAIT DECOUPER LES BITES AUTREMENT ! "
	}
}


// Fourni par la matrice pour l'épreuve
int main (int argc, char** argv)
{

	// Test Question 1 affiche m128
	__m128i a = _mm_set_epi8(10,20,30,40,1,2,3,4,10,20,30,40,1,2,3,4);
	affiche_xmm_i(a);
	__m128i b = _mm_set_epi16(10,20,30,40,1,2,3,4);
	affiche_xmm_i(b);
	__m128i c = _mm_set_epi32(10,20,30,40);
	affiche_xmm_i(c);
	// Fin test Question 1

	IplImage* img = NULL;

	if(argc!=2)
	{
	cerr << "Usage: " << argv[0] << " image" << endl;
	exit(1);
	}

	cout << "Ouverture de " << argv[1] << endl;
	img=cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE);	// chargement en mémoire de l'image depuis le fichier passé en paramtre
	if(!img)
	{
	cerr << "Could not load image file: " << argv[1] << endl;
	exit(2);
	}
	cvShowImage( "Affiche_origine", img);
	cout << argv[1] << " : " << img->height << "x" << img->width << endl;

	// création d'une image pour stocker l'image inversée
	IplImage* image_inverse = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	Inverse((unsigned char*) img->imageData, img->height, img->width, (unsigned char*) image_inverse->imageData);
	cvShowImage( "Affiche_inverse", image_inverse);

	// création d'une image pour stocker l'image inversée en SSE
	IplImage* image_inverse_sse = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	Inverse_sse((unsigned char*) img->imageData, img->height, img->width, (unsigned char*) image_inverse_sse->imageData);
	cvShowImage( "Affiche_inverse_sse", image_inverse_sse);

	// CHANGEMENT DE LUMINANCE SSE
	IplImage* image_luminance_sse = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	Luminance_sse( 140 , (unsigned char*) img->imageData, img->height, img->width, (unsigned char*) image_luminance_sse->imageData);
	cvShowImage( "Affiche_luminance_sse", image_luminance_sse);

	// création d'une image pour stocker l'image binarisée
	IplImage* image_binaire = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	binarise( 150, (unsigned char*) img->imageData, img->height, img->width, (unsigned char*) image_binaire->imageData);
	cvShowImage( "Affiche_binaire", image_binaire);

	// création d'une image pour stocker l'image binarisée SSE
	IplImage* image_binair = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	binarise_sse( 150, (unsigned char*) img->imageData, img->height, img->width, (unsigned char*) image_binair->imageData);
	cvShowImage( "Affiche_binaire_sse", image_binair);

	unsigned char min, max;
	min_et_max( (unsigned char*) img->imageData, img->height, img->width, min, max);
	cout << "min : " << (int)min << "   max : " << (int)max << endl;
	min = 255; max = 0 ;
	min_et_max_sse( (unsigned char*) img->imageData, img->height, img->width, min, max);
	cout << "min : " << (int)min << "   max : " << (int)max << endl;

	// création d'une image pour stocker l'image de recadrage dynamique
	IplImage* image_ = cvCreateImage (cvGetSize (img), IPL_DEPTH_8U, 1);
	recadrageDynamique    ( (unsigned char*) img->imageData, img->height, img->width, min, max, (unsigned char*) image_->imageData);
	cvShowImage( "Affiche_RecDyn    ", image_);

	// création d'une image pour stocker l'image de recadrage dynamique SSE
	// NE MARCHE PAS CONVENABLEMENT
	// PAS REUSSI A FAIRE REPASSER LES VALEURS DE 32 A 8 BITS
	// TRAITER EN 32 BITS TEL QUE JE L'AI FAIT NE FONCTIONNE PAS CORRECTEMENT
	IplImage* image_2           = cvCreateImage (cvGetSize (img), IPL_DEPTH_32S, 1);
	recadrageDynamique_sse( (unsigned char *) img->imageData, img->height, img->width, min, max,  (unsigned int*)image_2->imageData);
	cvShowImage( "Affiche_RecDyn_SSE", image_2);

	cvWaitKey();		// attend une touche clavier
	exit (0);
}
